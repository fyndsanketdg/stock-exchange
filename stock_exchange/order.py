from dataclasses import dataclass
from stock_exchange.stock import Stock
from enum import Enum
from datetime import datetime


class OrderType(Enum):
    BUY = "buy"
    SELL = "sell"

    @classmethod
    def get_inverse(cls, value):
        return cls.BUY if value == OrderType.BUY else OrderType.SELL


class OrderStatus(Enum):
    QUEUED = "in_queue"
    PROCESSING = "in_process"
    COMPLETED = "completed"


@dataclass
class Order:
    stock: Stock
    created: datetime.date
    type: OrderType
    quantity: int
    price: float
    status: OrderStatus = OrderStatus.QUEUED
