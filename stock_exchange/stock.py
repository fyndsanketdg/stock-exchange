from dataclasses import dataclass


@dataclass
class Stock:
    name: str
    symbol: str
    price: float

    @property
    def symbol(self) -> str:
        return self.symbol

    def __str__(self) -> str:
        return f"<{self.symbol}> <{self.price}>"

    def __repr__(self) -> str:
        return f"<{self.symbol}>"
